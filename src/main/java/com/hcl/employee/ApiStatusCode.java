package com.hcl.employee;

public class ApiStatusCode {

	public static final int PRODUCT_NOT_FOUND = 600;
	public static final int INVALID_DATA = 400;
	public static final int INVALID_CREDENTIALS = 400;
	public static final int VALIDATION_FAILED = 102;

}
