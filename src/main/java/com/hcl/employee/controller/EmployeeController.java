package com.hcl.employee.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.employee.dto.EmployeeDto;
import com.hcl.employee.model.Employee;
import com.hcl.employee.serviceImpl.EmployeeServiceImpl;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeServiceImpl employeeServiceImpl;

	@PostMapping("/employees")
	public ResponseEntity<Employee> addEmployee(@Valid @RequestBody Employee employee) {
		return new ResponseEntity<Employee>(employeeServiceImpl.addEmployee(employee), HttpStatus.OK);
	}

	@GetMapping("/employees")
	public ResponseEntity<List<String>> displayEmployee() {
		return new ResponseEntity<List<String>>(employeeServiceImpl.displayEmployeesByName(), HttpStatus.OK);

	}

	@GetMapping("/employees/name")
	public ResponseEntity<List<EmployeeDto>> displayEmployeeByDesignation() {
		return new ResponseEntity<List<EmployeeDto>>(employeeServiceImpl.displayEmployeesByNameAndDesignation(),
				HttpStatus.OK);

	}

	@GetMapping("/employees/salary")
	public ResponseEntity<List<EmployeeDto>> displayEmployeeBySalary() {
		return new ResponseEntity<List<EmployeeDto>>(employeeServiceImpl.displaySalary(), HttpStatus.OK);

	}

}
