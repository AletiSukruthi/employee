package com.hcl.employee.model;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employee")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int empId;

	@Column(name = "employee_name")
	private String name;

	@Column(name = "salary")
	public double salary;

	@Column(name = "designation")
	private String designation;

	@Column(name = "age")
	private int age;

	@Column(name = "phone_number")
	private Long phoneNumber;

}
