package com.hcl.employee.service;

import java.util.List;

import com.hcl.employee.dto.EmployeeDto;
import com.hcl.employee.model.Employee;

public interface IEmployeeService {

	public Employee addEmployee(Employee employee);

	public List<String> displayEmployeesByName();

	public List<EmployeeDto> displayEmployeesByNameAndDesignation();

	public List<EmployeeDto> displaySalary();

}
