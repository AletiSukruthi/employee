package com.hcl.employee.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.employee.dao.EmployeeRepository;
import com.hcl.employee.dto.EmployeeDto;
import com.hcl.employee.model.Employee;
import com.hcl.employee.service.IEmployeeService;

@Service
public class EmployeeServiceImpl implements IEmployeeService {
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

	@Autowired
	private EmployeeRepository employeeRepository;

	List<Employee> employeeList = new ArrayList<Employee>();
	List<EmployeeDto> employeeDtoList = new ArrayList<EmployeeDto>();

	@Override
	public Employee addEmployee(Employee employee) {
		// TODO Auto-generated method stub
		if (employee.getAge() < 18) {
			LOGGER.error("Age should be less than 18");
		    return null;
		}

		if (employee.getPhoneNumber().toString().length() != 10) {

			LOGGER.error("PhoneNumber length should be 10");
			return null;
		}
		return employeeRepository.save(employee);

	}

	@Override
	public List<String> displayEmployeesByName() {
		// TODO Auto-generated method stub

		System.out.println(employeeList.stream().filter(e -> e.getSalary() > 50000).map(Employee::getName));
		return employeeRepository.findAll().stream().filter(e -> e.getSalary() > 50000).map(Employee::getName)
				.collect(Collectors.toList());

	}

	@Override
	public List<EmployeeDto> displayEmployeesByNameAndDesignation() {
		// TODO Auto-generated method stub

		employeeDtoList.add(new EmployeeDto("Vikky", "software engineer", 10000.0));
		employeeDtoList.add(new EmployeeDto("sukruthi", "developer", 20000.0));
		employeeDtoList.add(new EmployeeDto("sukku", "testing", 15000.0));
		employeeDtoList.add(new EmployeeDto("chinnu", "support role", 10000.0));

		return employeeDtoList.stream().filter(emp -> emp.getSalary() < 20000).collect(Collectors.toList());

	}

	@Override
	public List<EmployeeDto> displaySalary() {
		// TODO Auto-generated method stub
		final double INCREMENT_SALARY = 10000;

		List<EmployeeDto> employee = displayEmployeesByNameAndDesignation().stream().map(emp -> {
			emp.setSalary(emp.getSalary() + INCREMENT_SALARY);
			return emp;

		}).collect(Collectors.toList());
		return employee;

	}

}
